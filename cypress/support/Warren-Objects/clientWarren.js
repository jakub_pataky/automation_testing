export class clientWarren {
    menuWarren(menuItem) {
        cy.get('#_navMenu').find('li').each( $listItem => {
            if($listItem[0].innerText === menuItem){
                cy.wrap($listItem).click()
            }
        })
    }

    /*
    * Profil Klienta
    * */

    riskQuestions(id, answer) {
        cy.get('.table-row').find(`[data-cy="rizikovyBtn${id}"]`).then(questionBtn => {
            cy.wrap(questionBtn).click()
            cy.wait(500)
            cy.get('.radio19').find('.spanBlock').then(answers => {
                cy.wrap(answers).eq(answer).click()
            })
            cy.get('[data-cy="ulozitForm"]').click()
            cy.wait(500)
        })
    }

    clientQuestions(id, answer) {
        cy.get('.table-row').find(`[data-cy="klientBtn${id}"]`).then(questionBtn => {
            cy.wrap(questionBtn).click()
            cy.wait(500)
            cy.get('.radio19').find('.spanBlock').then(answers => {
                cy.wrap(answers).eq(answer).click()
                cy.get('[data-cy="ulozitForm"]').click()
            })
            cy.wait(500)
        })
    }

    clientUnderTheQuestions0(id, answer, underQ1, underQ2, underQ3, underQ4) {
        cy.get('.table-row').find(`[data-cy="klientBtn${id}"]`).then(questionBtn => {
            cy.wrap(questionBtn).click()
            cy.wait(500)
            cy.get('.radio19').find('.spanBlock').then(answers => {
                cy.wrap(answers).eq(answer).click()
                cy.get('.mtop5').find('.table').then(underAnswer => {
                    cy.wait(500)
                    cy.wrap(underAnswer).find(`[data-cy="investicniDotaznikPodotazkaOption0${underQ1}"]`).click({force: true})
                    cy.wrap(underAnswer).find(`[data-cy="investicniDotaznikPodotazkaOption1${underQ2}"]`).click({force: true})
                    cy.wrap(underAnswer).find(`[data-cy="investicniDotaznikPodotazkaOption2${underQ3}"]`).click({force: true})
                    cy.wrap(underAnswer).find(`[data-cy="investicniDotaznikPodotazkaOption3${underQ4}"]`).click({force: true})
                })
                cy.get('[data-cy="ulozitForm"]').click()
            })
            cy.wait(500)
        })
    }

    clientUnderTheQuestions1(id, underQ1, underQ2, underQ3, underQ4) {
        cy.get('.table-row').find(`[data-cy="klientBtn${id}"]`).then(questionBtn => {
            cy.wrap(questionBtn).click()
            cy.wait(500)
                cy.get('.mtop5').find('.table').then(underAnswer => {
                    cy.wrap(underAnswer).find(`[data-cy="klientZkusenostiOption0${underQ1}"]`).click({force: true})
                    cy.wrap(underAnswer).find(`[data-cy="klientZkusenostiOption1${underQ2}"]`).click({force: true})
                    cy.wrap(underAnswer).find(`[data-cy="klientZkusenostiOption2${underQ3}"]`).click({force: true})
                    cy.wrap(underAnswer).find(`[data-cy="klientZkusenostiOption3${underQ4}"]`).click({force: true})
                })
                cy.get('[data-cy="ulozitForm"]').click()
            cy.wait(500)
        })
    }

    /*
    * Cile
    * */

    addGoal(name, invest, year, month){
        cy.get('.table-cell').find('.taLeft').then(goal => {
            cy.wrap(goal).find('[data-cy="pridatCilBtn"]').click()
            cy.get('.table').find('.table-row').then(input => {
                cy.wrap(input).find('[data-cy="nazevCile"]').type(name)
                cy.wrap(input).find('#Cil_UcelInvesticeTypEnum').select(invest)
                cy.wrap(input).find('[data-cy="budouciHodnotaCil"]').type('100000')
                cy.wrap(input).find('.calendar22').eq(0).then(calendar => {
                    cy.wrap(calendar).click()
                    cy.get('.datepicker').find('.datepicker-years').then(years => {
                        cy.wrap(years).find('.year').contains(year).then(months => {
                            cy.wrap(months).click()
                            cy.get('.datepicker-months').find('span').contains(month).click()
                        })
                    })
                })
                cy.get('[data-cy="ulozitForm"]').click()
            })
        })
    }

    /*
    * Kolik investovat
    * */

    investment(onceInvest, volume, regularInvest, jobRole, salary) {
        cy.get('.pright').find('.mtop').then(input => {
            cy.wrap(input).find('[data-cy="jednorazoveInvestice"]').type(onceInvest)
            cy.wrap(input).find('[data-cy="objemProstredku"]').type(volume)
        })
        cy.get('.pleft').find('.mtop').then(input => {
            cy.wrap(input).find('[data-cy="pravidelneInvestice"]').type(regularInvest)
            cy.wrap(input).find('[data-cy="oborCinnosti"]').type(jobRole)
            cy.wrap(input).find('[data-cy="rocniPrijem"]').clear().type(salary)
        })
    }

    /*
    * Realizace portfolia
    * */

    addContracts(type, company, product) {
        cy.get('.taLeft').find('[data-cy="pridatSmlouvuBtn"]').then(btn => {
            cy.wrap(btn).click()
        })
        cy.wait(500)
        cy.get('.table').find('.table-row').then(selelctRow => {
            cy.wrap(selelctRow).find('#VybranyProduktTypId').select(type)
            cy.wrap(selelctRow).find('#VybranaSpolecnostId').select(company)
            cy.wrap(selelctRow).find('#VybranyProduktId').select(product)
        })
        cy.get('[data-cy="ulozitForm"]').click()
        cy.wait(500)
    }

    addFonds(fond) {
        cy.get('.table').find('.table-row').then(arrow => {
            cy.wrap(arrow).find('.searchArrow').click()
        })
        cy.wait(500)
        cy.get('ul').find('.ui-menu-item').then(fondItem => {
            cy.wrap(fondItem).contains(fond).click()
        })
        cy.get('[data-cy="ulozitForm"]').click()
        cy.wait(500)
    }

    investFond(contractId, fondId, onceInvest, regularInvest){
        cy.get('.table').find('.table-row').then(selelctRow => {
            cy.wrap(selelctRow).find(`[data-cy="fondJednorazovaInvestice${contractId}${fondId}"]`).clear().type(onceInvest)
            cy.get(`[data-cy="fondPravidelnaInvestice${contractId}${fondId}"]`).clear().type(regularInvest)
        })
        cy.wait(500)
        cy.get('.bgGreyLight').find('.ptop').then(saveBtn => {
            cy.wrap(saveBtn).contains('ULOŽIT').click({force: true})
        })
    }

    amundiContract(contractId, fondId, initial, ending, onceInvest, regularInvest){
        cy.get('.table-row').find('.table-cell4').then(select => {
            cy.wrap(select).find(`[data-cy="smlouvaPocatecni${contractId}${fondId}"]`).select(initial)
        })
        cy.get('.table-row').find('.table-cell4').then(select => {
            cy.wrap(select).find(`[data-cy="smlouvaKoncova${contractId}${fondId}"]`).select(ending)
        })
        cy.get('.table').find('.table-row').then(selelctRow => {
            cy.wrap(selelctRow).find(`[data-cy="smlouvaJednorazovaInvestice${contractId}${fondId}"]`).clear().type(onceInvest)
        })
        cy.get('.table').find('.table-row').then(selelctRow => {
            cy.wrap(selelctRow).find(`[data-cy="smlouvaPravidelnaInvestice${contractId}${fondId}"]`).clear().type(regularInvest)
        })
        cy.wait(500)
        cy.get('.bgGreyLight').find('.ptop').then(saveBtn => {
            cy.wrap(saveBtn).contains('ULOŽIT').click({force: true})
        })
    }

    /*
    * Dokončení poradenství
    * */

    finishWarren(typeId, typeSelect, discountId, discountSelect) {
        cy.wait(1000)
        cy.get('.dtable').find('.dtd').then(select => {
            cy.wrap(select).find(`[data-cy="typyPoplatku${typeId}"]`).select(typeSelect)
            cy.wrap(select).find(`[data-cy="sleva${discountId}"]`).select(discountSelect)
        })
        cy.get('[data-cy="pokracovat"]').click({force: true})
        cy.wait(2000)
        cy.get('[data-cy="pokracovat"]').click({force: true})
    }
}

export const onClientWarren = new clientWarren()