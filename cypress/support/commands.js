/*
* Production
* */

Cypress.Commands.add('loginToAppProd', (login, pw) => {
    cy.visit('https://www.epartners.cz/fip/Analyzy/Nova')

    cy.get('tbody').find('tr').then(tableRow => {
        cy.wrap(tableRow).find('[name="txtUserName"]').type(login)
        cy.wrap(tableRow).find('[name="txtPassword"]').type(pw)
        cy.wrap(tableRow).find('[value="Přihlásit"]').click()
    })
})

Cypress.Commands.add('loginToAppPersonProd', (id, login, pw) => {
    cy.visit('https://www.epartners.cz/fip/Analyzy/' + id + '/osoby')

    cy.get('tbody').find('tr').then(tableRow => {
        cy.wrap(tableRow).find('[name="txtUserName"]').type(login)
        cy.wrap(tableRow).find('[name="txtPassword"]').type(pw)
        cy.wrap(tableRow).find('[value="Přihlásit"]').click()
    })
})

Cypress.Commands.add('loginToAppWarrenProd', (id, login, pw) => {
    cy.visit('https://www.epartners.cz/fip/investicni-poradenstvi/warren/' + id)

    cy.get('tbody').find('tr').then(tableRow => {
        cy.wrap(tableRow).find('[name="txtUserName"]').type(login)
        cy.wrap(tableRow).find('[name="txtPassword"]').type(pw)
        cy.wrap(tableRow).find('[value="Přihlásit"]').click()
    })
})

/*
* Stage
* */

Cypress.Commands.add('loginToAppStage', (login, pw) => {
    cy.visit('https://stageares.pdevelop.cz/fip20/Analyzy/Nova')

    cy.get('tbody').find('tr').then(tableRow => {
        cy.wrap(tableRow).find('[name="txtUserName"]').type(login)
        cy.wrap(tableRow).find('[name="txtPassword"]').type(pw)
        cy.wrap(tableRow).find('[value="Přihlásit"]').click()
    })
})

Cypress.Commands.add('loginToAppPersonStage', (id, login, pw) => {
    cy.visit('https://stageares.pdevelop.cz/fip20/Analyzy/' + id + '/osoby')

    cy.get('tbody').find('tr').then(tableRow => {
        cy.wrap(tableRow).find('[name="txtUserName"]').type(login)
        cy.wrap(tableRow).find('[name="txtPassword"]').type(pw)
        cy.wrap(tableRow).find('[value="Přihlásit"]').click()
    })
})

/*
* Sandbox
* */

Cypress.Commands.add('loginToAppSandbox', (login, pw) => {
    cy.visit('https://sandbox.epartners.cz/FiP-PB/Analyzy/Nova')

    cy.get('tbody').find('tr').then(tableRow => {
        cy.wrap(tableRow).find('[name="txtUserName"]').type(login)
        cy.wrap(tableRow).find('[name="txtPassword"]').type(pw)
        cy.wrap(tableRow).find('[value="Přihlásit"]').click()
    })
})

Cypress.Commands.add('loginToAppPersonSandbox', (id, login, pw) => {
    cy.visit('https://sandbox.epartners.cz/FiP-IP/Analyzy/' + id + '/osoby')

    cy.get('tbody').find('tr').then(tableRow => {
        cy.wrap(tableRow).find('[name="txtUserName"]').type(login)
        cy.wrap(tableRow).find('[name="txtPassword"]').type(pw)
        cy.wrap(tableRow).find('[value="Přihlásit"]').click()
    })
})

Cypress.Commands.add('loginToAppWarrenSandbox', (id, login, pw) => {
    cy.visit('https://sandbox.epartners.cz/FiP-IP/investicni-poradenstvi/warren/' + id)

    cy.get('tbody').find('tr').then(tableRow => {
        cy.wrap(tableRow).find('[name="txtUserName"]').type(login)
        cy.wrap(tableRow).find('[name="txtPassword"]').type(pw)
        cy.wrap(tableRow).find('[value="Přihlásit"]').click()
    })
})

/*
* Login
* */

Cypress.Commands.add('loginToPageCZ', () => {

    cy.get('tbody').find('tr').then(tableRow => {
        cy.wrap(tableRow).find('[name="txtUserName"]').type('milan.poradce')
        cy.wrap(tableRow).find('[name="txtPassword"]').type('MPoradce456')
        cy.wrap(tableRow).find('[value="Přihlásit"]').click()
    })
})

Cypress.Commands.add('loginToPageSK', () => {

    cy.get('tbody').find('tr').then(tableRow => {
        cy.wrap(tableRow).find('[name="txtUserName"]').type('marek.master')
        cy.wrap(tableRow).find('[name="txtPassword"]').type('MMaster456')
        cy.wrap(tableRow).find('[value="Přihlásit"]').click()
    })
})