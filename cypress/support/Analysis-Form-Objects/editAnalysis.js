const delay = { delay: 35 }

export class editAnalysis {

    clickEditButton(id) {
        cy.get('#persons').find('.sezalozkou').then(div => {
            cy.wrap(div[id]).find('.edit-orange-large').click({force: true})
        })
    }

    /*
    * Kliknutie na Osobní doklad
    * */
    // { Občanský průkaz, Pas, Řidičský průkaz}
    selectIdCard(idCard) {
        cy.get('table').contains('caption', 'OSOBNÍ DOKLAD').siblings('tbody').then(select => {
            cy.wrap(select).find('#VM_IdCardType').select(1).should('have.contain', idCard)
        })
    }

    /*
    * Základní údaje
    * */

    personallyInfo(firstName, lastName) {
        cy.get('table').should('have.contain', 'OSOBNÍ ÚDAJE').find('tbody').then(tableRow => {
            cy.wrap(tableRow).find('#VM_FirstName').clear().type(firstName, delay)
            cy.wrap(tableRow).find('#VM_LastName').clear().type(lastName, delay)
            cy.wrap(tableRow).find('[name="VM.MaritalStatusId"]').select('1').should('have.value', '1')
        })
    }

    idCard(num, city, date) {
        cy.get('table').should('have.contain', 'OSOBNÍ DOKLAD').find('tbody').then(tableRow => {
            cy.wrap(tableRow).find('[name="VM.IdentityCard.Number"]').clear().type(num, delay)
            cy.wrap(tableRow).find('[name="VM.IdentityCard.IssuedBy"]').clear().type(city, delay)
            cy.wrap(tableRow).find('[name="VM.IdentityCard.ValidTo"]').clear().type(date, delay)
        })
    }

    other(iban0, iban1, iban2) {
        cy.get('table').contains( 'OSTATNÍ').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('[data-cy="cisloUctuPrvni"]').clear().type(iban0, delay)
            cy.wrap(tableRow).find('[data-cy="cisloUctuDruhy"]').clear().type(iban1, delay)
            cy.wrap(tableRow).find('[data-cy="cisloUctuTreti"]').clear().type(iban2, delay)
        })
    }

    income(ratio, income0, income1) {
        cy.get('table').contains('caption','PŘÍJMY').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('select').select(ratio)
            cy.wrap(tableRow).find('[data-cy="prijem-0"]').clear().type(income0, delay)
            cy.wrap(tableRow).find('[data-cy="prijem-1"]').clear().type(income1, delay)
        })
    }

    expenses(expenses) {
        cy.get('table').contains( 'caption', 'VÝDAJE').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('[data-cy="vydaje"]').clear().type(expenses, delay)
        })
    }

    /*
   * Kontaktní údaje
   * */

    email(mail) {
        cy.get('table').contains('thead', 'EMAIL').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('#VM_ContactEmail_Address').clear().type(mail)
        })
    }

    phone(tel) {
        cy.get('table').contains('thead', 'TELEFON').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('#VM_ContactPhone_Number').clear().type(tel)
        })
    }

    address(street, streetNum, town, postalCode) {
        cy.get('table').contains( 'thead', 'ADRESA').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('#VM_PermanentAddress_Street').clear().type(street, delay)
            cy.wrap(tableRow).find('#VM_PermanentAddress_LandRegistryNumber').clear().type(streetNum, delay)
            cy.wrap(tableRow).find('#VM_PermanentAddress_Town').clear().type(town, delay)
            cy.wrap(tableRow).find('#VM_PermanentAddress_PostalCode').clear().type(postalCode, delay)
        })
    }
}

export const onEditAnalysis = new editAnalysis()