const delay = { delay: 35 }
export class newAnalysis {

    clickTabPersons(person) {
        cy.get('#person-control-buttons').find('a').then(btn => {
            cy.wrap(btn).contains('Přidat novou osobu').click()
            cy.get('.filtrItemSub > div').should('have.contain', person)
            cy.get('.filtrItemSub > div').each(($div) => {
                if ($div[0].innerText === person) {
                    cy.wrap($div).click()
                }
            })
        })
    }

    /*
    * Click on the IdCard
    * */
    // { Občanský průkaz, Pas, Řidičský průkaz }
    selectIdCard(idCard) {
        cy.get('table').contains('caption', 'OSOBNÍ DOKLAD').siblings('tbody').then(select => {
            cy.wrap(select).find('#VM_IdCardType').select(1).should('have.contain', idCard)
        })
    }

    /*
    * Základní údaje
    * */

    identificationAnalysis(analysisName) {
        cy.get('table').should('have.contain', 'IDENTIFIKACE ANALÝZY').find('tbody').then(tableRow => {
            cy.wrap(tableRow).find('#AddModel_Text').type(analysisName, delay)
        })
    }

    personallyInfo(firstName, lastName) {
        cy.get('table').should('have.contain', 'OSOBNÍ ÚDAJE').find('tbody').then(tableRow => {
            cy.wrap(tableRow).find('#VM_FirstName').type(firstName, delay)
            cy.wrap(tableRow).find('#VM_LastName').type(lastName, delay)
            cy.wrap(tableRow).find('[name="VM.MaritalStatusId"]').select('1').should('have.value', '1')
        })
    }

    identificationNum(num) {
        cy.get('table').should('have.contain', 'IDENTIFIKAČNÍ ČÍSLA OSOBY').find('tbody').then(tableRow => {
            cy.wrap(tableRow).find('[data-cy="rodneCislo"]').type(num, delay)
        })
    }

    idCard(num, city, date) {
        cy.get('table').should('have.contain', 'OSOBNÍ DOKLAD').find('tbody').then(tableRow => {
            cy.wrap(tableRow).find('[name="VM.IdentityCard.Number"]').type(num, delay)
            cy.wrap(tableRow).find('[name="VM.IdentityCard.IssuedBy"]').type(city, delay)
            cy.wrap(tableRow).find('[name="VM.IdentityCard.ValidTo"]').type(date, delay)
        })
    }

    amlInfo(bornCity) {
        cy.get('table').should('have.contain', 'AML INFORMACE').find('tbody').then(tableRow => {
            cy.wrap(tableRow).find('#VM_BirthPlace').type(bornCity, delay)
        })
    }

    other(iban0, iban1, iban2) {
        cy.get('table').contains( 'OSTATNÍ').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('[data-cy="cisloUctuPrvni"]').type(iban0, delay)
            cy.wrap(tableRow).find('[data-cy="cisloUctuDruhy"]').type(iban1, delay)
            cy.wrap(tableRow).find('[data-cy="cisloUctuTreti"]').type(iban2, delay)
        })
    }

    otherIbanSk(iban) {
        cy.get('table').contains( 'OSTATNÍ').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('[data-cy="cisloUctuIban"]').type(iban, delay)
        })
    }

    income(ratio, income0, income1) {
        cy.get('table').contains('caption','PŘÍJMY').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('select').select(ratio)
            cy.wrap(tableRow).find('[data-cy="prijem-0"]').type(income0, delay)
            cy.wrap(tableRow).find('[data-cy="prijem-1"]').type(income1, delay)
        })
    }

    expenses(expenses) {
        cy.get('table').contains( 'caption', 'VÝDAJE').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('[data-cy="vydaje"]').type(expenses, delay)
        })
    }

    /*
   * Kontaktní údaje
   * */

    email(mail) {
        cy.get('table').contains('thead', 'EMAIL').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('#VM_ContactEmail_Address').type(mail)
        })
    }

    phone(tel) {
        cy.get('table').contains('thead', 'TELEFON').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('#VM_ContactPhone_Number').type(tel)
        })
    }

    address(street, streetNum, town, postalCode) {
        cy.get('table').contains( 'thead', 'ADRESA').siblings('tbody').then(tableRow => {
            cy.wrap(tableRow).find('#VM_PermanentAddress_Street').type(street, delay)
            cy.wrap(tableRow).find('#VM_PermanentAddress_LandRegistryNumber').type(streetNum, delay)
            cy.wrap(tableRow).find('#VM_PermanentAddress_Town').type(town, delay)
            cy.wrap(tableRow).find('#VM_PermanentAddress_PostalCode').type(postalCode, delay)
        })
    }

    /*
   * Right Menu
   * */

    routeToPath(nameItem) {
        cy.get('div').find('#menuRight').then(menu => {
            cy.wrap(menu).find('#menuRightTab').click()
            cy.wrap(menu).find('#ctxSouvisejciNastroje').should('be.visible').then(menuItem => {
                cy.wrap(menuItem).contains(nameItem).click()
            })
        })
    }

    /*
    * Division Analysis
    * */

    splitAnalysis() {
        cy.get('#personsBlocks').find('.mtop').then(btn => {
            cy.wrap(btn).find('#submitSplit').should('contain', 'POKRAČOVAT').click()
        })
        cy.loginToPage('milan.poradce', 'MPoradce456')
        cy.get('div').find('.mtop').then(btn => {
            cy.wrap(btn).find('#btnSubmit').should('contain', 'POTVRDIT ROZDĚLENÍ').click()
        })
    }

    /*
    * Merge Analysis
    * */

    mergeAnalysis(name) {
        cy.get('#mergeDashboard').find('input').then(input => {
            cy.wrap(input)
                .should('have.attr','placeholder', 'Příjmení klienta nebo id klienta ve FiPu')
                .type(name, {delay: 500})
        })
    }

    /*
    * Merge Duplicate Analysis
    * */

    mergeDuplicateAnalysis() {
        cy.get('#duplicity-table').find('#duplicity-body').then(btn => {
            cy.wrap(btn).find('a').contains('SLOUČIT').click()
        })
        cy.loginToPageCZ()
        cy.get('#anlaBlocks').find('.mtop20').then(btn => {
            cy.wrap(btn).find('a').contains('POTVRDIT SLOUČENÍ').click()
        })
    }
}

export const onNewAnalysis = new newAnalysis()