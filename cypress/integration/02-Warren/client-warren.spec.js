/// <reference types="cypress" />
import { onClientWarren } from "../../support/Warren-Objects/clientWarren";

// Here is needed use command .only after it command

describe('Create a Warren', () => {

    /*
    * CZ Login: user - milan.poradce, pass - MPoradce456
    * SK Login: user - marek.master, pass - MMaster456
    * */

    before('Login', () => {
        // id, user, pass
        cy.loginToAppWarrenProd('873869', 'milan.poradce', 'MPoradce456')
    })


    it.only('Profil Klienta', () => {
        /**
         * navigation for the warren
         * **/

        // menuItem
        onClientWarren.menuWarren('Profil klienta')

        /**
         * main part
         * **/

        // id, answer
        onClientWarren.riskQuestions( 0, 0)
        onClientWarren.riskQuestions( 1, 3)
        onClientWarren.riskQuestions( 2, 1)

        // id, answer
        onClientWarren.clientQuestions('0', '1')
        // id, answer, underQ1, underQ2, underQ3, underQ4
        onClientWarren.clientUnderTheQuestions0('1', '1', '0', '2', '1', '3')
        onClientWarren.clientUnderTheQuestions1('2', '4', '4', '4', '4')
    });

    it('Cile', () => {
        /**
         * navigation for the warren
         * **/

        // menuItem
        onClientWarren.menuWarren('Cíle')

        /**
         * main part
         * **/

        // name, invest, year, month
        onClientWarren.addGoal('Nazov Cile', 'Zabezpečení budoucích potřeb rodiny (studium dětí apod.)', '2027', 'Srp')
    });

    it('Kolik investovat', () => {
        /**
         * navigation for the warren
         * **/

        // menuItem
        onClientWarren.menuWarren('Kolik investovat')

        /**
         * main part
         * **/

        // oneceInvest, volume, regularInvest, jobRole, salary
        onClientWarren.investment('1 000 000', '1 000 000','10 000','IT', '300 000')
    });

    it('Realizace portfolia', () => {
        /**
         * navigation for the warren
         * **/

        // menuItem
        onClientWarren.menuWarren('Realizace portfolia')

        /**
         * main part
         * **/

        // type, company, product
        onClientWarren.addContracts('Podílové fondy','Amundi Czech Republic investiční společnost, a. s.','Cool Invest')
        // fond
        // onClientWarren.addFonds('(CZ0008043874) Nemovitostní fond Trigea – Trigea nemovitostní fond, SICAV, a.s. (CZK)')
        // contractId, fondId, onceInvest, regularInvest
        // onClientWarren.investFond(0,0,'10 000 Kč','1 000 Kč')

        onClientWarren.amundiContract(0,0,'Konzervativní','Konzervativní', 0, '1 000 Kč')
    })

    it('Dokončení poradenství', () => {
        /**
         * navigation for the warren
         * **/

        // menuItem
        onClientWarren.menuWarren('Dokončení poradenství')

        /**
         * main part
         * **/

        // typeId, typeSelect, discountId, discountSelect
        onClientWarren.finishWarren(0,'Průběžný (z každé platby)',0,'30 %')
    })
})