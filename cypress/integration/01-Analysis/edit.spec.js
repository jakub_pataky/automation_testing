/// <reference types="cypress" />

import { onEditAnalysis } from "../../support/Analysis-Form-Objects/editAnalysis";

describe('Create a Person Partner', () => {

    /*
    * CZ Login: user - milan.poradce, pass - MPoradce456
    * SK Login: user - marek.master, pass - MMaster456
    * */

    before('Login', () => {
        // id, user, pass
        cy.loginToAppPerson('724179', 'milan.poradce', 'MPoradce456')
    })

    it('Editácia', () => {

        /*
        * Click on the current person
        * */

        // ID is person which will be edited (First Person equal number 0)
        onEditAnalysis.clickEditButton('0')
        cy.wait(15000)

        /*
        * Základní údaje
        * */

        // onEditAnalysis.personallyInfo('Ludka', 'Pataky')
        // onEditAnalysis.idCard('123446788', 'Praha', '15.3.2024')
        // onEditAnalysis.other('2700', '2700', '2700')
        // onEditAnalysis.income('Hlavní pracovní poměr', '10000', '15000')

        /*
        * Kontaktní údaje
        * */

        cy.get('ul').find('li').then(btn => {
            cy.wrap(btn).contains('Kontaktní údaje').click()
        })
        onEditAnalysis.email('testiy@test.cz')
        // onEditAnalysis.phone('544315235')
        onEditAnalysis.address('hocico Street', '15', 'Praha', '59822')

        /*
       * Submitting Form
       * */
        cy.get('button[type="submit"]').click({ force: true })
    })
})