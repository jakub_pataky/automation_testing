/// <reference types="cypress" />

import { onNewAnalysis } from "../../../support/Analysis-Form-Objects/newAnalysis";

describe('Create a Person Partner', () => {

    /*
    * CZ Login: user - milan.poradce, pass - MPoradce456
    * SK Login: user - marek.master, pass - MMaster456
    * */

    before('Login', () => {
        // id, user, pass
        cy.loginToAppPersonStage('762517', 'milan.poradce', 'MPoradce456')
    })

    it('Kliknutie na správnu osobu, Základní údaje a Kontaktní údaje', () => {
        /*
        * Preklik na správnu osobu
        * */
        onNewAnalysis.clickTabPersons('Přidat člena domácnosti')

        /*
        * Základní údaje
        * */

        onNewAnalysis.personallyInfo('Miroslav', 'Hromas')
        onNewAnalysis.identificationNum('6706178699')
        onNewAnalysis.selectIdCard('Občanský průkaz')
        // num, city, date
        onNewAnalysis.idCard('FE157852', 'Praha', '15.3.2024')
        onNewAnalysis.amlInfo('Praha')
        cy.wait(2000)
        /*
       * Submitting Form
       * */
        cy.get('button[type="submit"]').click({ force: true })
    })
})