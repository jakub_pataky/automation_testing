/// <reference types="cypress" />

import { onNewAnalysis } from "../../../support/Analysis-Form-Objects/newAnalysis";

describe('Create a Person Partner', () => {

    /*
    * CZ Login: user - milan.poradce, pass - MPoradce456
    * SK Login: user - marek.master, pass - MMaster456
    * */

    before('Login', () => {
        // id, user, pass
        cy.loginToAppPersonProd('484830', 'milan.poradce', 'MPoradce456')
    })

    it('Kliknutie na správnu osobu, Základní údaje a Kontaktní údaje', () => {
        /*
        * Preklik na správnu osobu
        * */

        onNewAnalysis.clickTabPersons('Přidat partnera')

        /*
        * Základní údaje
        * */

        onNewAnalysis.personallyInfo('Paulína', 'Minaříková')
        onNewAnalysis.identificationNum('7653135347')
        // num, city, date
        onNewAnalysis.idCard('FB149874', 'Zvoleneves', '15.3.2024')
        onNewAnalysis.amlInfo('Zvoleneves')
        // iban0, iban1, iban2
        onNewAnalysis.other('2700', '2700', '2700')
        // ratio, income0, income1
        onNewAnalysis.income('Hlavní pracovní poměr', '10000', '15000')

        /*
        * Kontaktní údaje
        * */

        cy.get('ul').find('li').then(btn => {
            cy.wrap(btn).contains('Kontaktní údaje').click()
        })
        onNewAnalysis.email('paulinaminarikova@armyspy.com')
        onNewAnalysis.phone('735484275')

        /*
       * Submitting Form
       * */
        cy.get('button[type="submit"]').click({ force: true })
    })
})