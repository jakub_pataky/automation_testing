/// <reference types="cypress" />

import { onNewAnalysis } from "../../../support/Analysis-Form-Objects/newAnalysis";

describe('Create a Person Partner', () => {

    /*
    * CZ Login: user - milan.poradce, pass - MPoradce456
    * SK Login: user - marek.master, pass - MMaster456
    * */

    before('Login', () => {
        // id, user, pass
        cy.loginToAppPerson('440788', 'milan.poradce', 'MPoradce456')
    })

    it('Kliknutie na správnu osobu, Základní údaje a Kontaktní údaje', () => {
        /*
        * Preklik na správnu osobu
        * */
        onNewAnalysis.clickTabPersons('Přidat dítě')

        /*
        * Základní údaje
        * */

        onNewAnalysis.personallyInfo('Kateřina', 'Pataky')
        onNewAnalysis.identificationNum('0758255344')
        onNewAnalysis.amlInfo('Praha')
        cy.wait(2000)

        /*
       * Submitting Form
       * */
        cy.get('button[type="submit"]').click({ force: true })
    })
})