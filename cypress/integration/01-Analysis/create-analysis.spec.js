/// <reference types="cypress" />

import { onNewAnalysis } from "../../support/Analysis-Form-Objects/newAnalysis"

describe('Create a Analysis', () => {

    /*
    * CZ Login: user - milan.poradce, pass - MPoradce456
    * SK Login: user - marek.master, pass - MMaster456
    * */

    before('Login', () => {
        // user, pass
       cy.loginToAppSandbox('milan.poradce', 'MPoradce456')
    })

    it('Základní údaje a Kontaktní údaje', () => {

        /*
        * Základní údaje
        * */

        onNewAnalysis.identificationAnalysis('Test Analyza Sandbox')
        onNewAnalysis.personallyInfo('Vladimír', 'Novotný')
        onNewAnalysis.identificationNum('9211275359')
        // num, city, date
        onNewAnalysis.idCard('FG253414', 'Nasavrky', '12.12.2026')
        onNewAnalysis.amlInfo('Nasavrky')
        // iban0, iban1, iban2
        onNewAnalysis.other('2700', '2700', '2700')
        // ratio, income0, income1
        onNewAnalysis.income('Hlavní pracovní poměr', '10000', '15000')
        onNewAnalysis.expenses('3000')


        /*
        * Kontaktní údaje
        * */


        cy.get('ul').find('li').then(btn => {
            cy.wrap(btn).contains('Kontaktní údaje').click()
        })
        onNewAnalysis.email('vladimirnovotny@armyspy.com')
        onNewAnalysis.phone('469202647')
        // street, streetNum, town, postalCode
        onNewAnalysis.address('Jiráskova', '498', 'Nasavrky', '53825')

        /*
        * Submitting Form
        * */
        cy.get('button[type="submit"]').click({ force: true })
    })
})