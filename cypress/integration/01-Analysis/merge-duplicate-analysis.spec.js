/// <reference types="cypress" />

import { onNewAnalysis } from "../../support/Analysis-Form-Objects/newAnalysis"

describe('Division a Analysis', () => {

    /*
    * CZ Login: user - milan.poradce, pass - MPoradce456
    * SK Login: user - marek.master, pass - MMaster456
    * */

    before('Login', () => {
        // user, pass
        cy.loginToAppPersonProd('496413', 'milan.poradce', 'MPoradce456')
    })

    it('route to the merge', () => {
        onNewAnalysis.routeToPath('Sloučení duplicitních analýz')
    })

    it('finish to the merge analysis', () => {
        onNewAnalysis.mergeDuplicateAnalysis()
    })
})